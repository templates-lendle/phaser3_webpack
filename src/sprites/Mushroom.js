import Phaser from 'phaser'

class Mushroom extends Phaser.GameObjects.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'mushroom')
    scene.add.existing(this)
  }

  update () {
    this.angle += 10
  }
}
export {Mushroom};