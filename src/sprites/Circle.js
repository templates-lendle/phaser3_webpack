import Phaser from 'phaser'

class Circle extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'circle');
    scene.add.existing(this);
    scene.physics.world.enable(this);
  }

  update () {
    this.angle += 10;
  }
}

export {Circle};