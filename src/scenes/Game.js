import Phaser from 'phaser'
import { Logo } from '../images/Logo'
import {Mushroom}  from '../sprites/Mushroom.js'
import {Circle} from '../sprites/Circle.js'

class Game extends Phaser.Scene {
  constructor () {
    super({ key: 'Game' })
  }

  create () {
    this.logo = new Logo(this, 400, 150)
    this.tweens.add({
      targets: this.logo,
      y: 450,
      duration: 2000,
      ease: 'Power2',
      yoyo: true,
      loop: -1
    })

    this.mushroom = new Mushroom(this, 0, 0)
    Phaser.Display.Align.In.Center(this.mushroom, this.add.zone(400, 300, 800, 600))

    this.circle = new Circle(this, 200, 200);
    this.circle.body.setCollideWorldBounds(true);
    let self=this;
    this.input.on('pointermove', function(e){
      self.physics.moveTo(self.circle, e.x, e.y);
      // self.circle.setVelocity(0, 0);
      // if(self.circle.x!=e.x){
      //   if(self.circle.x>e.x){
      //     self.circle.setVelocityX(-50);
      //   }else{
      //     self.circle.setVelocityX(50);
      //   }
      // }
      // if(self.circle.y!=e.y){
      //   if(self.circle.y>e.y){
      //     self.circle.setVelocityY(-50);
      //   }else{
      //     self.circle.setVelocityY(50);
      //   }
      // }
      //this.physics.moveTo(this.circle, e.x, e.y);
    });
  }

  update () {
    this.mushroom.update()
    this.circle.update()
  }
}

export {Game};